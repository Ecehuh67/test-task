import axios from 'axios';

const createAPI = () => {
  const api = axios.create({
    baseURL: `http://www.boredapi.com/api/activity`,
    timeout: 5000
  });

  const onSuccess = (response) => {
    return response;
  };

  const onFail = (err) => {
    throw err;
  };

  api.interceptors.response.use(onSuccess, onFail);

  return api;
};

export {createAPI};
