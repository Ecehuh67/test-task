import App from './index';
import {connect} from "react-redux";
import {ActionCreator} from "../../reducer/actions";
import {Operations} from "../../reducer/operations";

const mapStateToProps = (state) => {
  return {
    currentStep: state.currentStep,
  };
};

const mapDispatchToProps = (dispatch) => ({
  onChangeStep() {
    dispatch(ActionCreator.changeStep());
  },
  onChangeParticipates(value) {
    dispatch(ActionCreator.changeParticipates(value));
  },
  onChangePrice(value) {
    dispatch(ActionCreator.changePrice(value));
  },
  onChangeType(value) {
    dispatch(ActionCreator.changeType(value));
  },
  onResetHandler() {
    dispatch(ActionCreator.resetSteps());
  },
  onReturnBack() {
    dispatch(ActionCreator.returnBack());
  },
  loadActivity() {
    dispatch(Operations.loadActivity());
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
