import WelcomeScreen from '../welcome-screen';

const App = (props) => {
  const {
    currentStep,
    onChangeStep,
    onChangeParticipates,
    onChangePrice,
    onChangeType,
    onResetHandler,
    onReturnBack,
  } = props;


  return (
    <WelcomeScreen
      onDataChange={onChangeStep}
      currentStep={currentStep}
      onChangeParticipates={onChangeParticipates}
      onChangePrice={onChangePrice}
      onChangeType={onChangeType}
      onResetHandler={onResetHandler}
      onReturnBack={onReturnBack}
    />
  );
};

App.propTypes = {
  currentStep: PropTypes.number.isRequired,
  onChangeStep: PropTypes.func.isRequired,
  onChangeParticipates: PropTypes.func.isRequired,
  onChangePrice: PropTypes.func.isRequired,
  onChangeType: PropTypes.func.isRequired,
  onResetHandler: PropTypes.func.isRequired,
  onReturnBack: PropTypes.func.isRequired,
};

export default App;
