import App from './index';
import {Provider} from "react-redux";
import configureStore from "redux-mock-store";

const mockStore = configureStore([]);

describe(`render App`, () => {
  it(`Render steps correctly`, () => {
    const store = mockStore({
      currentStep: 0,
      participants: 0,
      type: ``,
      price: ``,
      activity: ``
    });
    const tree = renderer
            .create(
                <Provider store={store}>
                  <App
                    currentStep={1}
                    onChangeStep={() => {}}
                    onChangeParticipates={() => {}}
                    onChangePrice={() => {}}
                    onChangeType={() => {}}
                    onResetHandler={() => {}}
                    onReturnBack={() => {}}
                  />
                </Provider>
            )
            .toJSON();

    expect(tree).toMatchSnapshot();
  });
});
