import FinalScreen from './index';
import {connect} from "react-redux";
import {Operations} from "../../reducer/operations";

const mapStateToProps = (state) => {
  return {
    activity: state.activity
  };
};

const mapDispatchToProps = (dispatch) => ({
  loadActivity() {
    dispatch(Operations.loadActivity());
  },
});
export default connect(mapStateToProps, mapDispatchToProps)(FinalScreen);
