export default class FinalScreen extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const {loadActivity} = this.props;
    loadActivity();
  }

  render() {
    const {activity} = this.props;

    return (
      <>
        <p className="main-section__passage">According to yours preferences the best activity now</p>
        <p className="main-section__passage--activity">{activity} </p>
      </>
    );
  }
}

FinalScreen.propTypes = {
  activity: PropTypes.string,
  loadActivity: PropTypes.func.isRequired
};
