import FinalScreen from './index';

describe(`render FinalScreen`, () => {
  it(`Render screen correctly`, () => {
    const tree = renderer
            .create(
                <FinalScreen
                  loadActivity={() => {}}
                  activity={`Go to a gym`}
                />
            )
        .toJSON();

    expect(tree).toMatchSnapshot();
  });
});
