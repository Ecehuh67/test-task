import {generateListTemplate} from '../../utils';
import {MAX_AMOUNT_OF_ACTIVITY, WAY_OF_ACTIVITY, DURATION, TYPE_OF_ACTIVITY} from "../../consts";
import FinalScreen from "../final-screen/index.connect";

const WelcomeScreen = (props) => {
  const {
    currentStep,
    onDataChange,
    onChangeParticipates,
    onChangePrice,
    onChangeType,
    onResetHandler,
    onReturnBack
  } = props;

  const randomTypes = TYPE_OF_ACTIVITY.sort(() => Math.random() - 0.5).slice(0, MAX_AMOUNT_OF_ACTIVITY);

  return (
    <section className="main-section">

      {currentStep < 3 &&
                <p className="main-section__passage--introduction">You&apos;ve had a really bad day, so now it&apos;s time for
                    relaxing!
                </p>
      }

      {currentStep === 0 &&
                <>
                  <p className="main-section__passage">Please, choose way of rest:</p>
                  {generateListTemplate(WAY_OF_ACTIVITY, onDataChange, onChangeParticipates)}
                </>

      }

      {currentStep === 1 &&
                <>
                  <p className="main-section__passage">preferable time of action</p>
                  {generateListTemplate(DURATION, onDataChange, onChangePrice)}
                </>
      }

      {currentStep === 2 &&
            <>
              <p className="main-section__passage">preferable kind of activity</p>

              {generateListTemplate(randomTypes, onDataChange, onChangeType)}
            </>
      }

      {currentStep === 3 &&
        <>
          <FinalScreen
          />
        </>
      }

      <ul className="main-section_step-back step-back">
        <li
          className={currentStep > 2 ? `step-back__item visually-hidden` : `step-back__item`}
          onClick={onReturnBack}
        >
        </li>
        <li
          className="step-back__item"
          onClick={onResetHandler}
        >
          <svg
            className="re"
            width="20"
            heigth="20"
          >
            <use xlinkHref="#restart"></use>
          </svg>
        </li>
      </ul>
    </section>
  );
};

WelcomeScreen.propTypes = {
  currentStep: PropTypes.number.isRequired,
  onDataChange: PropTypes.func.isRequired,
  onChangeParticipates: PropTypes.func.isRequired,
  onChangePrice: PropTypes.func.isRequired,
  onChangeType: PropTypes.func.isRequired,
  onResetHandler: PropTypes.func.isRequired,
  onReturnBack: PropTypes.func.isRequired,
};

export default WelcomeScreen;
