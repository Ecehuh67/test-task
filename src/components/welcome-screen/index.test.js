import WelcomeScreen from './index';

describe(`render WelcomeScreen`, () => {
  it(`Render screen correctly`, () => {
    const tree = renderer
            .create(
                <WelcomeScreen
                  currentStep={0}
                  onDataChange={() => {}}
                  onChangeParticipates={() => {}}
                  onChangePrice={() => {}}
                  onChangeType={() => {}}
                  onResetHandler={() => {}}
                  onReturnBack={() => {}}
                />
            )
            .toJSON();

    expect(tree).toMatchSnapshot();
  });
});
