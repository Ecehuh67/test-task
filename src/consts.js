export const MAX_AMOUNT_OF_ACTIVITY = 5;
export const MaxRangeOfPrice = 9;
export const WAY_OF_ACTIVITY = [`alone`, `with friends`];
export const DURATION = [
  `less then 10 min`,
  `more then 10 min`
];
export const TYPE_OF_ACTIVITY = [
  `education`,
  `recreational`,
  `social`,
  `diy`,
  `charity`,
  `cooking`,
  `relaxation`,
  `music`,
  `busywork`
];

export const MAX_AMOUNT_OF_PARTICIPATES = 5;
