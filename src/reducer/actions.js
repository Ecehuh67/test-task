const ActionType = {
  CHANGE_STEP: `CHANGE_STEP`,
  CHANGE_PARTICIPATES: `CHANGE_PARTICIPATES`,
  CHANGE_PRICE: `CHANGE_PRICE`,
  CHANGE_TYPE: `CHANGE_TYPE`,
  RESET_STEPS: `RESET_STEPS`,
  RETURN_BACK: `RETURN_BACK`,
  LOAD_ACTIVITY: `LOAD_ACTIVITY`
};

const ActionCreator = {
  changeStep: () => {
    return {
      type: ActionType.CHANGE_STEP,
      payload: null
    };
  },
  changeParticipates: (value) => {
    return {
      type: ActionType.CHANGE_PARTICIPATES,
      payload: value
    };
  },
  changePrice: (value) => {
    return {
      type: ActionType.CHANGE_PRICE,
      payload: value
    };
  },
  changeType: (value) => {
    return {
      type: ActionType.CHANGE_TYPE,
      payload: value
    };
  },
  resetSteps: () => {
    return {
      type: ActionType.RESET_STEPS,
      payload: null
    };
  },
  returnBack: () => {
    return {
      type: ActionType.RETURN_BACK,
      payload: null
    };
  },
  loadActivity: (activity) => {
    return {
      type: ActionType.LOAD_ACTIVITY,
      payload: activity
    };
  },
};

export {ActionType, ActionCreator};
