import {ActionCreator} from './actions';

const Operations = {
  loadActivity: () => (dispatch, getState, api) => {
    const state = getState();
    const query = `?participants=${state.participants}&type=${state.type}&price=${state.price}`;

    return api.get(query)
      .then((response) => {
        const activity = response.data.activity;
        const error = response.data.error;

        if (error === undefined) {
          dispatch(ActionCreator.loadActivity(activity));
        } else {
          dispatch(ActionCreator.loadActivity(error));
        }
      })
      .catch((err) => {
        throw err;
      });
  }
};

export {Operations};
