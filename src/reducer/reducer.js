import {ActionType} from "./actions";
import {getParticipates, getPrice} from '../utils';

const initialState = {
  currentStep: 0,
  participants: 0,
  type: ``,
  price: '',
  activity: ``
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionType.CHANGE_STEP:
            return {
                ...state,
                currentStep: state.currentStep + 1
            };
        case ActionType.CHANGE_PARTICIPATES:
            return {
                ...state,
                participants: getParticipates(action.payload)
            };
        case ActionType.CHANGE_PRICE:
            return {
                ...state,
                price: getPrice(action.payload)
            };
        case ActionType.CHANGE_TYPE:
            return {
                ...state,
                type: action.payload
            };
        case ActionType.RESET_STEPS:
            return initialState;
        case ActionType.RETURN_BACK:
            return {
                ...state,
                currentStep: state.currentStep > 0 ? state.currentStep - 1 : state.currentStep
            };
        case ActionType.LOAD_ACTIVITY:
          return {
              ...state,
              activity: action.payload
          };
        default:
            break;
    }

    return state;
};

export {reducer};
