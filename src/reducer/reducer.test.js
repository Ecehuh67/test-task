import {reducer} from "./reducer";
import {ActionType} from './actions';

it(`Reducer without additional parameters should return initial state`, () => {
  expect(reducer(void 0, {})).toEqual({
    currentStep: 0,
    participants: 0,
    type: ``,
    price: ``,
    activity: ``
  });
});

it(`Reducer should change number of step`, () => {
  expect(reducer(
      {
        currentStep: 0,
        participants: 0,
        type: ``,
        price: ``,
        activity: ``
      },
      {
        type: ActionType.CHANGE_STEP,
        payload: null
      })
  ).toEqual(
      {
        currentStep: 1,
        participants: 0,
        type: ``,
        price: ``,
        activity: ``
      }
  );
});

it(`Reducer should change number of participants`, () => {
  expect(reducer(
      {
        currentStep: 0,
        participants: 0,
        type: ``,
        price: ``,
        activity: ``
      },
      {
        type: ActionType.CHANGE_PARTICIPATES,
        payload: `alone`
      })
  ).toEqual(
      {
        currentStep: 0,
        participants: 1,
        type: ``,
        price: ``,
        activity: ``
      }
  );
});

it(`Reducer should change number of price`, () => {
  expect(reducer(
      {
        currentStep: 0,
        participants: 0,
        type: ``,
        price: ``,
        activity: ``
      },
      {
        type: ActionType.CHANGE_PRICE,
        payload: `less then 10 min`
      })
  ).toEqual(
      {
        currentStep: 0,
        participants: 0,
        type: ``,
        price: `0.1`,
        activity: ``
      }
  );
});

it(`Reducer should change type of activity`, () => {
  expect(reducer(
      {
        currentStep: 0,
        participants: 0,
        type: ``,
        price: ``,
        activity: ``
      },
      {
        type: ActionType.CHANGE_TYPE,
        payload: `business`
      })
  ).toEqual(
      {
        currentStep: 0,
        participants: 0,
        type: `business`,
        price: ``,
        activity: ``
      }
  );
});

it(`Reducer should reset steps`, () => {
  expect(reducer(
      {
        currentStep: 2,
        participants: 0,
        type: ``,
        price: ``,
        activity: ``
      },
      {
        type: ActionType.RESET_STEPS,
        payload: null
      })
  ).toEqual(
      {
        currentStep: 0,
        participants: 0,
        type: ``,
        price: ``,
        activity: ``
      }
  );
});

it(`Reducer should return back for one step`, () => {
  expect(reducer(
      {
        currentStep: 2,
        participants: 0,
        type: ``,
        price: ``,
        activity: ``
      },
      {
        type: ActionType.RETURN_BACK,
        payload: null
      })
  ).toEqual(
      {
        currentStep: 1,
        participants: 0,
        type: ``,
        price: ``,
        activity: ``
      }
  );
});

it(`Reducer should load an activity`, () => {
  expect(reducer(
      {
        currentStep: 0,
        participants: 0,
        type: ``,
        price: ``,
        activity: ``
      },
      {
        type: ActionType.LOAD_ACTIVITY,
        payload: `go swimming`
      })
  ).toEqual(
      {
        currentStep: 0,
        participants: 0,
        type: ``,
        price: ``,
        activity: `go swimming`
      }
  );
});
