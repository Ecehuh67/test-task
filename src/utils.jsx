import {MAX_AMOUNT_OF_PARTICIPATES, WAY_OF_ACTIVITY, DURATION, MaxRangeOfPrice} from "./consts";

export const generateListTemplate = (arrayData, onDataChange, onSubmit) => {
  return (
    <ul className="main-section_list types-list">
      {
        arrayData.map((el, i) => {
          return (
            <li className="types-list__element" key={i}>
              <button
                data-value={el}
                onClick={(evt) => {
                  const value = evt.currentTarget.dataset.value;
                  onDataChange();
                  onSubmit(value);
                }}
              >{el}
              </button>
            </li>
          );
        })
      }
    </ul>
  );
};

const getRandomNumber = (maxCount) => {
  const number = Math.round(Math.random() * maxCount);

  return number;
};

export const getParticipates = (value) => {
  let amount = 0;
  switch (true) {
    case value === WAY_OF_ACTIVITY[0]:
      amount = 1;
      break;
    case value === WAY_OF_ACTIVITY[1]:
      amount = getRandomNumber(MAX_AMOUNT_OF_PARTICIPATES);
      break;
  }

  return amount;
};

export const getPrice = (value) => {
  let amount = 0;
  switch (true) {
    case value === DURATION[0]:
      amount = `0.1`;
      break;
    case value === DURATION[1]:
      amount = `0.${getRandomNumber(MaxRangeOfPrice)}`;
      break;
  }

  return amount;
};


